<?php
namespace dadasign\feedParser;

/**
 * Generated by PHPUnit_SkeletonGenerator on 2016-05-29 at 05:31:23.
 */
class TabLineParserTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var TabLineParser
     */
    protected $object;
    /**
     *
     * @var resource
     */
    private $fh;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new TabLineParser;
        $this->fh = fopen(dirname(__FILE__).'/data/simple.tab', 'r');
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        fclose($this->fh);
    }

    /**
     * @covers dadasign\feedParser\TabLineParser::getLine
     * @todo   Implement testGetLine().
     */
    public function testGetLine()
    {
        $out1 = $this->object->getLine($this->fh);
        $this->assertEquals($out1[0], 'Col1');
        $this->assertEquals($out1[1], 'Col2');
        $out2 = $this->object->getLine($this->fh);
        $this->assertEquals($out2[0], 'Value1a');
        $this->assertEquals($out2[1], 'Value2a');
        $out3 = $this->object->getLine($this->fh);
        $this->assertEquals($out3[0], 'Value1b');
        $this->assertEquals($out3[1], 'Value2b');
    }
}
