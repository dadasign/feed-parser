<?php
namespace dadasign\feedparser;
/**
 *
 * @author dadasign
 */
interface LineParserInterface {
    /**
     * Get a single line of a feed into an array of values.
     * @param resource $line
     */
    public function getLine(&$fileRes);
}