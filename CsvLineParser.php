<?php
namespace dadasign\feedparser;
/**
 * Parse CSV files.
 *
 * @author dadasign
 */
class CsvLineParser implements LineParserInterface{
    public function getLine(&$fileRes) {
        $lineItems = fgetcsv($fileRes);
        if($lineItems===false){
            fclose($fileRes);
            return false;
        }
        return $lineItems;
    }
}
