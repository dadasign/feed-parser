<?php
namespace dadasign\feedparser;

/**
 * Parse tab files.
 *
 * @author dadasign
 */
class TabLineParser implements LineParserInterface{
    public function getLine(&$res){
        $this->lineRaw = fgets($res);
        if($this->lineRaw===false){
            fclose($res);
            return false;
        }
        $line_filtered = trim($this->lineRaw,"\n\r");
        return explode("\t", $line_filtered);
    }
    
}
