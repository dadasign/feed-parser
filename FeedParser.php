<?php
namespace dadasign\feedparser;

/**
 * Parse feeds, using a given LineParser.
 *
 * @author dadasign
 */
class FeedParser {

    private $rootDir;

    /**
     * @var LineParserInterface
     */
    private $lineParser;
    /**
     * Opened file handle.
     * @var resource
     */
    private $fileHandle;
    /**
     * Data of the header of the tab file (first line).
     * @var array
     */
    public $header;
    /**
     * Strict mode - throw exceptions if a feed line with different value count than the header is encountered.
     * @var boolean 
     */
    private $strict=true;
    /**
     * Map to use for the columns.
     * @var array
     */
    protected $columnMap;
    /** @var int */
    private $currentLine=0;
    
    /**
     * 
     * @param \utils\feedParser\LineParserInterface $lineParser
     * @param string $rootDir
     */
    public function __construct(LineParserInterface $lineParser, $rootDir) {
        $this->lineParser = $lineParser;
        $this->rootDir = (substr($rootDir, -1)==='/' || $rootDir==='')?$rootDir:$rootDir.'/';
    }
    /**
     * Load a file to use.
     * @param string $filename
     * @param boolean $has_header Set if the first line should be treated as a header or as data.
     * @param array $column_map If a coulmn map is provided and is associative it will map keys matching header values to provided values. If the array is not associative it will map columns in the order they appear.
     */
    public function loadFile($filename,$has_header,array $column_map=null){
        $this->fileHandle = fopen($this->rootDir.$filename, 'r');
        if($has_header){
            $this->header =  $this->lineParser->getLine($this->fileHandle);
        }else{
            $this->header = null;
        }
        if($column_map!==null){
            $this->setColumnMap($column_map);
        }
        $this->currentLine=0;
    }
    /**
     * Assign a column map. If the file has a header you can pass an associative array, mapping header names to output names.
     * @param type $map
     */
    public function setColumnMap($map){
        if($map===null || !$this->is_assoc($map) || $this->header===null){
            $this->columnMap = $map;
        }else{
            foreach($this->header as $h){
                $out[$h]=$h;
            }
            foreach($map as $name=>$item){
                if(isset($out[$name])){
                    $out[$name]=$item;
                }
            }
            $this->columnMap = array_values($out);
        }
    }
    /**
     * Returns data for a line. If a column map was provided it will return an associative array, otherwise fields in the order of appearance are returned.
     * @return boolean|array
     */
    public function getLine(){
        $tab_array = $this->lineParser->getLine($this->fileHandle);
        if($tab_array===false){
            return false;
        }
        $this->currentLine++;
        if($this->columnMap!==null){
            $out=array();
            $c=0;
            foreach($tab_array as $item){
                $out[$this->columnMap[$c]]=$item;
                $c++;
            }
            return $out;
        }else if($this->header!==null){
            if($this->strict){
                if(count($this->header)===count($tab_array)){
                    return array_combine($this->header, $tab_array);
                }else{
                    throw new \Exception('Header count does not match value count: '.count($this->header).' vs '.count($tab_array).' on line '.$this->currentLine.'<br/>'.json_encode($tab_array));
                }
            }else{
                if(count($this->header)===count($tab_array)){
                    return array_combine($this->header, $tab_array);
                }else{
                    $c=0;
                    $out = array();
                    foreach($tab_array as $item){
                        if(!empty($this->header[$c])){
                            $out[$this->header[$c]]=$item;
                        }
                        $c++;
                    }
                    return $out;
                }
            }
        }else{
            return $tab_array;
        }
    }
    /**
     * Check if array is asssociative.
     * @param type $array
     * @return boolean
     */
    private function is_assoc(array $array) {
        return (bool)count(array_filter(array_keys($array), 'is_string'));
    }
    /**
     * Is strict mode enabled.
     * @return boolean
     */
    function getStrict() {
        return $this->strict;
    }
    /**
     * Get feed header.
     * @return string[]
     */
    function getHeader() {
        return $this->header;
    }
    /**
     * Set strict mode - if true throw exceptions on line count different than header count.
     * @param boolean $strict
     */
    function setStrict($strict) {
        $this->strict = $strict==true;
    }
}
