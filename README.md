# Feed Parser#

A very basic CSV and Tab feed parsing library.

### Features ###

- Allows mapping to associative arrays based on header or mapping parameter passed;
- Extendable to handle feeds with other data object per line formats;
- Strict mode allows ensuring values count matches header on every line.

### Basic Usage ###
Parsing a simple tab file having a header:
```
#!php
$parser = new FeedParser(new TabLineParser(),  '/path_to_your_feed_files');
$parser->loadFile('my_data.tab',true);
while($row = $parser->getLine()){
    var dump($row);//Your data array.
}
```

Parsing with mapping to an associative array:
```
#!php
$parser = new FeedParser(new TabLineParser(),  '/path_to_your_feed_files');
$parser->loadFile('my_data.tab',false,array("First Column","Second Column"));
while($row = $parser->getLine()){
    echo $row["First Column"];
}
```